
package com.ntc.databaseproject.service;

import com.ntc.databaseproject.dao.UserDao;
import com.ntc.databaseproject.model.User;

/**
 *
 * @author L4ZY
 */
public class UserService {
    public User login(String name, String password)
    {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if(user.getPassword().equals(password))
        {
            return user;
        }
        return null;
    }
            
    
}
