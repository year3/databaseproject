package com.ntc.databaseproject;

import com.ntc.databaseproject.dao.UserDao;
import com.ntc.databaseproject.helper.DatabaseHelper;
import com.ntc.databaseproject.model.User;

/**
 *
 * @author L4ZY
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        //System.out.println(userDao.getAll());
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        //User user1=userDao.get(1);
        //System.out.println(user1);

        //User user2= new User("user3", "1234", 2,"F");
        //User insertedUser= userDao.save(user2);
        //insertedUser.setGender("M");
        //user1.setGender("F");
        //userDao.update(user1);
        //User updateUser = userDao.get(user1.getId());
        //System.out.println(updateUser);
        //userDao.delete(user1);
        //for (User u : userDao.getAll()) {
        //    System.out.println(u);
        //}
        for (User u : userDao.getAll("user_name like 'u%'", "user_name asc , user_gender desc")) {
            System.out.println(u);
        }

        DatabaseHelper.close();

    }

}
