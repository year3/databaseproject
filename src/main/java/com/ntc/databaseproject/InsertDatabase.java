package com.ntc.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.spi.DirStateFactory;

/**
 *
 * @author L4ZY
 */
public class InsertDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish. ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        String sql = "INSERT INTO category(category_id,category_name) VALUES (?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "CTG004");
            stmt.setString(2, "candy");
            int status = stmt.executeUpdate();
            //ResultSet key=stmt.getGeneratedKeys();
           // key.next();
           // System.out.println(""+ key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        //close
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

}
