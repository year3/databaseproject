
package com.ntc.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author L4ZY
 */
public class UpdateDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish. ");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update
        String sql = "UPDATE category SET category_name=? WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(2, "CTG004");
            stmt.setString(1, "Candys");
            int status = stmt.executeUpdate();
            //ResultSet key=stmt.getGeneratedKeys();
           // key.next();
           // System.out.println(""+ key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        //close
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
    
}
