
package com.ntc.databaseproject;

import com.ntc.databaseproject.model.User;
import com.ntc.databaseproject.service.UserService;

/**
 *
 * @author L4ZY
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1", "1234");
        if(user!=null && user!=null)
        {
            System.out.println("Welcome user :" + user.getName());
        }else
        {
            System.out.println("ERROR");
        }
    }
    
}
